Utilisation de différents Design Pattern dans plusieurs situations exemple.

Factory Method :
Une entreprise de logistique assure des services de transport maritimes et terrestres. Le transport maritime
s’effectue actuellement via des bateaux, le transport terrestre s’effectue avec des camions.

Observer :
Application météo

Strategy :
Plusieurs constructeurs construisent des maisons. Il y a un constructeur français, un autre
américain et un troisième néerlandais. Ils peuvent utiliser des matériaux différents : le parpaing, les briques et le
bois. En utilisant le patron Stratégie on change le matériau de construction de chaque constructeur à l’exécution du
programme, de manière dynamique.
